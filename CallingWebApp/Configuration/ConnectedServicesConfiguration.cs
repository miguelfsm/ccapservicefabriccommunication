﻿namespace CallingWebApp.Configuration
{
    public class ConnectedServicesConfiguration
    {
        public string ValuesServiceUri { get; set; }
        public string ValuesEndpoint { get; set; }
    }
}
