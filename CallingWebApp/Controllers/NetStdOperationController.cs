﻿using System;
using System.Threading.Tasks;
using CallingWebApp.Configuration;
using HttpCommunicationClientNetStd;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.ServiceFabric.Services.Communication.Client;

namespace CallingWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/NetStdOperation")]
    public class NetStdOperationController : Controller
    {
        private readonly ConnectedServicesConfiguration _servicesConfiguration;
        private readonly IHttpCommunicationServicePartitionClientFactoryNetStd
            _httpCommunicationServicePartitionClientFactory;

        public NetStdOperationController(IHttpCommunicationServicePartitionClientFactoryNetStd httpCommunicationServicePartitionClientFactory, IOptions<ConnectedServicesConfiguration> servicesConfiguration)
        {
            _httpCommunicationServicePartitionClientFactory = httpCommunicationServicePartitionClientFactory;
            _servicesConfiguration = servicesConfiguration.Value;
        }
        // GET: api/NetStdOperation
        [HttpGet]
        public async Task<string> Get()
        {
            // call service using our HttpClient
            var partitionClient = _httpCommunicationServicePartitionClientFactory.Create();

            var response = await partitionClient.InvokeWithRetryAsync(async c => await c.HttpClient.GetAsync(_servicesConfiguration.ValuesEndpoint));

            var result = await response.Content.ReadAsStringAsync();

            return "From .net std: " + result; 
        }
    }
}
