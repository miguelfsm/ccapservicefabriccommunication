﻿using System.Threading.Tasks;
using HttpCommunicationClient;
using Microsoft.AspNetCore.Mvc;
using CallingWebApp.Configuration;
using Microsoft.Extensions.Options;

namespace CallingWebApp.Controllers
{
    [Produces("application/json")]
    [Route("api/SomeOperation")]
    public class SomeOperationController : Controller
    {
        private readonly ConnectedServicesConfiguration _servicesConfiguration;
        private readonly IHttpCommunicationServicePartitionClientFactory
            _httpCommunicationServicePartitionClientFactory;

        public SomeOperationController(IHttpCommunicationServicePartitionClientFactory httpCommunicationServicePartitionClientFactory, IOptions<ConnectedServicesConfiguration> servicesConfiguration)
        {
            _httpCommunicationServicePartitionClientFactory = httpCommunicationServicePartitionClientFactory;
            _servicesConfiguration = servicesConfiguration.Value;
        }

        // GET: api/SomeOperation
        [HttpGet]
        public async Task<string>  Get()
        {
            // might do some logic here...

            // call service using our HttpClient
            var partitionClient = _httpCommunicationServicePartitionClientFactory.Create();

            var response = await partitionClient.InvokeWithRetryAsync(async c => await c.HttpClient.GetAsync(_servicesConfiguration.ValuesEndpoint));

            var result = await response.Content.ReadAsStringAsync();

            return result; //  { "value1", "value2" };
        }
    }
}
