﻿using CallingWebApp.Configuration;
using HttpCommunicationClient;
using HttpCommunicationClientNetStd;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CallingWebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<CommunicationClientConfiguration>(Configuration.GetSection("CommunicationClientConfiguration"));
            services.Configure<CommunicationClientConfigurationNetStd>(Configuration.GetSection("CommunicationClientConfigurationNetStd"));
            services.Configure<ConnectedServicesConfiguration>(Configuration.GetSection("ConnectedServicesConfiguration"));

            services.AddMvc();

            services.AddServiceFabricHttpCommunication(Configuration);
            services.AddServiceFabricHttpCommunicationNetStd(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
