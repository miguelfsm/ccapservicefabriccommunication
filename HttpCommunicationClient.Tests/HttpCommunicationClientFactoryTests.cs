﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;
using Moq;
using Shouldly;

namespace HttpCommunicationClient.Tests
{
    [TestClass()]
    public class HttpCommunicationClientFactoryTests
    {
        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullLoggerFactory_ThrowsArgumentNullException()
        {
            var clientConfiguration = new Mock<IOptions<CommunicationClientConfiguration>>();

            new HttpCommunicationClientFactoryTestableWrapper(null, null,
                clientConfiguration.Object, null);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullClientConfiguration_ThrowsArgumentNullException()
        {
            var loggerFactory = new Mock<ILoggerFactory>();

            new HttpCommunicationClientFactoryTestableWrapper(null, null, null, loggerFactory.Object);
        }

        [TestMethod()]
        public void Constructor_CorrectParameters_InstanceIsCreated()
        {
            var clientConfiguration = new Mock<IOptions<CommunicationClientConfiguration>>();
            clientConfiguration.Setup(x => x.Value).Returns(new CommunicationClientConfiguration()
            {
                RequestTimeout = new TimeSpan(0, 0, 10)
            });
            var loggerFactory = new Mock<ILoggerFactory>();

            var unitUnderTest = new HttpCommunicationClientFactoryTestableWrapper(null, null,
                clientConfiguration.Object, loggerFactory.Object);

            unitUnderTest.ServiceResolver.ShouldNotBeNull();
            unitUnderTest.ExceptionHandlers.ShouldNotBeNull();
        }

        [TestMethod()]
        public void ValidateClient_NullClient_ReturnsFalse()
        {
            var clientConfiguration = new Mock<IOptions<CommunicationClientConfiguration>>();
            clientConfiguration.Setup(x => x.Value).Returns(new CommunicationClientConfiguration()
            {
                RequestTimeout = new TimeSpan(0, 0, 10)
            });
            var loggerFactory = new Mock<ILoggerFactory>();

            var unitUnderTest = new HttpCommunicationClientFactoryTestableWrapper(null, null,
                clientConfiguration.Object, loggerFactory.Object);

            unitUnderTest.ValidateClient(null).ShouldBeFalse();
        }

        [TestMethod()]
        public void ValidateClient_ClientInstance_ReturnsTrue()
        {
            var clientConfiguration = new Mock<IOptions<CommunicationClientConfiguration>>();
            clientConfiguration.Setup(x => x.Value).Returns(new CommunicationClientConfiguration()
            {
                RequestTimeout = new TimeSpan(0, 0, 10)
            });
            var loggerFactory = new Mock<ILoggerFactory>();

            var unitUnderTest = new HttpCommunicationClientFactoryTestableWrapper(null, null,
                clientConfiguration.Object, loggerFactory.Object);

            unitUnderTest.ValidateClient(new HttpCommunicationClient(new Uri("http://www.google.com"), new TimeSpan(0, 0, 10)))
                .ShouldBeTrue();
        }

        [TestMethod()]
        [DataRow("")]
        [DataRow(null)]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ValidateClient_Nullpoit_ThrowsArgumentNullException(string endpoint)
        {
            var clientConfiguration = new Mock<IOptions<CommunicationClientConfiguration>>();
            clientConfiguration.Setup(x => x.Value).Returns(new CommunicationClientConfiguration()
            {
                RequestTimeout = new TimeSpan(0, 0, 10)
            });
            var loggerFactory = new Mock<ILoggerFactory>();

            var unitUnderTest = new HttpCommunicationClientFactoryTestableWrapper(null, null,
                clientConfiguration.Object, loggerFactory.Object);

            unitUnderTest.ValidateClient(endpoint,
                new HttpCommunicationClient(new Uri("http://www.google.com"), new TimeSpan(0, 0, 10)));
        }

        [TestMethod()]
        [DataRow("blabla")]
        [DataRow("httpp://www.google.com")]
        [DataRow("htt://www.google.com")]
        public void ValidateClient_InvalidEndpoit_ReturnsFalse(string endpoint)
        {
            var clientConfiguration = new Mock<IOptions<CommunicationClientConfiguration>>();
            clientConfiguration.Setup(x => x.Value).Returns(new CommunicationClientConfiguration()
            {
                RequestTimeout = new TimeSpan(0, 0, 10)
            });
            var loggerFactory = new Mock<ILoggerFactory>();

            var unitUnderTest = new HttpCommunicationClientFactoryTestableWrapper(null, null,
                clientConfiguration.Object, loggerFactory.Object);

            unitUnderTest.ValidateClient(endpoint, new HttpCommunicationClient(new Uri("http://www.google.com"), new TimeSpan(0, 0, 10) ))
                .ShouldBeFalse();
        }

        [TestMethod()]
        [DataRow("http://localhost/serviceendpoint")]
        [DataRow("http://localhost/serviceendpoint/")]
        [DataRow("http://localhost/serviceendpoint//")]
        [DataRow("http://localhost")]
        [DataRow("http://localhost:80")]
        [DataRow("http://localhost/")]
        [DataRow("http://localhost:80/")]
        [DataRow("http://+:80")]
        public async Task CreateClientAsync_DifferentEndpointFormats_BaseAddressHasSingleTrailingSlash(string endpoint)
        {
            var clientConfiguration = new Mock<IOptions<CommunicationClientConfiguration>>();
            clientConfiguration.Setup(x => x.Value).Returns(new CommunicationClientConfiguration()
            {
                RequestTimeout = new TimeSpan(0, 0, 10)
            });
            var loggerFactory = new Mock<ILoggerFactory>();
            loggerFactory.Setup(x => x.CreateLogger(It.IsAny<string>())).Returns(new ConsoleLogger("test logger",
                (s, level) => false, false));

            var unitUnderTest = new HttpCommunicationClientFactoryTestableWrapper(null, null,
                clientConfiguration.Object, loggerFactory.Object);

            var client = await unitUnderTest.CreateClientAsync(endpoint, CancellationToken.None);
            // Assert
            client.HttpClient.BaseAddress.AbsoluteUri.ShouldEndWith("/");
        }
    }

    public class HttpCommunicationClientFactoryTestableWrapper : HttpCommunicationClientFactory
    {
        public HttpCommunicationClientFactoryTestableWrapper(ServicePartitionResolver servicePartitionResolver, IEnumerable<IExceptionHandler> exeptionHandlers, IOptions<CommunicationClientConfiguration> clientConfiguration, ILoggerFactory loggerFactory) 
            : base(servicePartitionResolver, exeptionHandlers, clientConfiguration, loggerFactory)
        {
        }

        public new bool ValidateClient(HttpCommunicationClient client)
        {
            return base.ValidateClient(client);
        }

        public new bool ValidateClient(string endpoint, HttpCommunicationClient client)
        {
            return base.ValidateClient(endpoint, client);
        }

        public new Task<HttpCommunicationClient> CreateClientAsync(string endpoint,
            CancellationToken cancellationToken)
        {
            return base.CreateClientAsync(endpoint, cancellationToken);
        }
    }
}