﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shouldly;

namespace HttpCommunicationClient.Tests
{
    [TestClass]
    public class HttpCommunicationClientTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Constructor_IncorrectTimeSpan_ThrowsArgumentOutOfRangeException()
        {
            new HttpCommunicationClient(new Uri("http://www.google.com"), new TimeSpan(0, 0, -1));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullBaseAddress_ThrowsArgumentNullException()
        {
            new HttpCommunicationClient(null,  new TimeSpan(0, 0, 10));
        }

        [TestMethod]
        public void Constructor_CorrectParameters_HttpClientIsAvailable()
        {
            var uriAddress = new Uri("http://www.google.com");
            var timeSpan = new TimeSpan(0, 0, 10);
            
            var unitUndertest = new HttpCommunicationClient(uriAddress, timeSpan);
            
            unitUndertest.HttpClient.BaseAddress.ShouldBe(uriAddress);
            unitUndertest.HttpClient.Timeout.ShouldBe(timeSpan);
        }
    }
}
