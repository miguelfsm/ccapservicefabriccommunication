﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using HttpCommunicationClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Services.Communication.Client;
using Moq;
using Shouldly;

namespace HttpCommunicationClient.Tests
{
    [TestClass()]
    public class HttpCommunicationServicePartitionClientFactoryTests
    {
        [TestMethod()]
        public void Create_ReturnsHttpCommunicationServicePartitionClient()
        {
            var _communicationClientFactory = new Mock<ICommunicationClientFactory<HttpCommunicationClient>>();
            var _serviceUri = new Uri("fabric:mockSF/Service");

            var unitUnderTest =
                new HttpCommunicationServicePartitionClientFactory(_communicationClientFactory.Object, _serviceUri);
            var result = unitUnderTest.Create();

            result.Factory.ShouldBe(_communicationClientFactory.Object);
            result.ServiceUri.ShouldBe(_serviceUri);
        }
    }
}