﻿using System;

namespace HttpCommunicationClient
{
    public class CommunicationClientConfiguration
    {
        public TimeSpan RequestTimeout { get; set; } = TimeSpan.FromSeconds(10);
    }
}