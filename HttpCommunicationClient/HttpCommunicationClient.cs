﻿using System;
using System.Fabric;
using System.Net.Http;
using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClient
{
    /// <summary>
    /// Implements ICommunicationClient by wrapping an HttpClient object
    /// </summary>
    public class HttpCommunicationClient : ICommunicationClient, IDisposable
    {
        public HttpClient HttpClient { get; }

        public HttpCommunicationClient(Uri baseAddress, TimeSpan requestTimeout)
        {
            ThrowIfArgumentsAreIncorrect(baseAddress, requestTimeout);

            HttpClientHandler httpClientHandler = new HttpClientHandler
            {
                AllowAutoRedirect = false
            };

            HttpClient = new HttpClient(httpClientHandler)
            {
                BaseAddress = baseAddress,
                Timeout = requestTimeout 
            };
        }

        private static void ThrowIfArgumentsAreIncorrect(Uri baseAddress, TimeSpan requestTimeout)
        {
            if (requestTimeout.Duration() <= TimeSpan.Zero)
            {
                throw new ArgumentOutOfRangeException(nameof(requestTimeout));
            }

            if (baseAddress == null)
            {
                throw new ArgumentNullException(nameof(baseAddress));
            }
        }

        /// <summary>
        /// Gets or Sets the Resolved service partition which was used when this client was created.
        /// </summary>
        public ResolvedServicePartition ResolvedServicePartition { get; set; }
        
        /// <summary>
        /// Gets or Sets the name of the listener in the replica or instance to which the client is connected to
        /// </summary>
        public string ListenerName { get; set; }

        /// <summary>
        /// Gets or Sets the service endpoint to which the client is connected to.
        /// </summary>
        public ResolvedServiceEndpoint Endpoint { get; set; }

        public void Dispose()
        {
            HttpClient?.Dispose();
        }
    }
}