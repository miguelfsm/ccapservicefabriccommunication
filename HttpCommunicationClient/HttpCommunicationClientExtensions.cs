﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClient
{
    public static class HttpCommunicationClientExtensions
    {
        public static IServiceCollection AddServiceFabricHttpCommunication(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.AddTransient(x => ServicePartitionResolver.GetDefault()); //Transient – Created every time they are requested
            services.AddTransient<IExceptionHandler, HttpCommunicationClientExceptionHandler>();
            services.AddSingleton<IHttpCommunicationClientFactory, HttpCommunicationClientFactory>(); //Singleton – Created only for the first request.
            services
                .AddSingleton<IHttpCommunicationServicePartitionClientFactory,
                    HttpCommunicationServicePartitionClientFactory>(
                    (ctx) =>
                    {
                        var communicationClientFactory =
                            ctx.GetService<IHttpCommunicationClientFactory>();
                        var serviceUri = new Uri(configuration["ConnectedServicesConfiguration:ValuesServiceUri"]);
                        return new HttpCommunicationServicePartitionClientFactory(communicationClientFactory,
                            serviceUri);
                    }
                );

            return services;
        }
    }
}