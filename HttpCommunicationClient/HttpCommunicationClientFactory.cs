﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClient
{
    /// <summary>
    ///     From MSDN: The client factory is primarily responsible for creating communication clients. For clients that
    ///     don't maintain a persistent connection, such as an HTTP client, the factory only needs to create and return the
    ///     client.
    ///     Other protocols that maintain a persistent connection, such as some binary protocols, should also be validated
    ///     by the factory to determine whether the connection needs to be re-created.
    /// </summary>
    /// <see cref="https://docs.microsoft.com/en-us/azure/service-fabric/service-fabric-reliable-services-communication" />
    public class HttpCommunicationClientFactory : CommunicationClientFactoryBase<HttpCommunicationClient>,
        IHttpCommunicationClientFactory
    {
        private readonly CommunicationClientConfiguration _clientConfiguration;
        private readonly ILogger _logger;

        public HttpCommunicationClientFactory(ServicePartitionResolver servicePartitionResolver,
            IEnumerable<IExceptionHandler> exeptionHandlers,
            IOptions<CommunicationClientConfiguration> clientConfiguration, ILoggerFactory loggerFactory)
            : base(servicePartitionResolver, exeptionHandlers)
        {
            ThrowIfArgumentsAreIncorrect(clientConfiguration, loggerFactory);

            _logger = loggerFactory.CreateLogger(nameof(HttpCommunicationClientFactory));
            _clientConfiguration = clientConfiguration.Value;
        }

        private static void ThrowIfArgumentsAreIncorrect(IOptions<CommunicationClientConfiguration> clientConfiguration, ILoggerFactory loggerFactory)
        {
            if (loggerFactory == null)
            {
                throw new ArgumentNullException(nameof(loggerFactory));
            }

            if (clientConfiguration == null || clientConfiguration.Value == null)
            {
                throw new ArgumentNullException(nameof(clientConfiguration));
            }
        }

        /// <summary>
        /// Connection oriented transports can use this method to indicate that the client is no longer connected to the service.
        /// </summary>
        /// <param name="client">An HttpCommunicationClient instance. This is called from the base class</param>
        /// <returns>Returns true if the client is still valid.</returns>
        protected override bool ValidateClient(HttpCommunicationClient client)
        {
            return client != null;
        }

        /// <summary>
        /// Returns true if the client is still valid and connected to the endpoint specified in the parameter.
        /// </summary>
        /// <param name="endpoint">the endpoint used to create/used in the client</param>
        /// <param name="client">An HttpCommunicationClient instance. This is called from the base class</param>
        /// <returns>Returns true if the client is still valid and connected to the endpoint specified in the parameter.</returns>
        protected override bool ValidateClient(string endpoint, HttpCommunicationClient client)
        {
            var endpointUri = CreateEndpointUri(endpoint);
            return client != null && client.HttpClient.BaseAddress == endpointUri;
        }

        protected override Task<HttpCommunicationClient> CreateClientAsync(string endpoint,
            CancellationToken cancellationToken)
        {
            _logger.LogInformation(new EventId(), "Creating HttpClient...");
            var endpointUri = CreateEndpointUri(endpoint);
            return Task.FromResult(new HttpCommunicationClient(endpointUri, _clientConfiguration.RequestTimeout));
        }

        protected override void AbortClient(HttpCommunicationClient client)
        {
            client?.Dispose();
        }

        private Uri CreateEndpointUri(string endpoint)
        {
            if (string.IsNullOrEmpty(endpoint))
                throw new ArgumentNullException(nameof(endpoint));

            // BaseAddress must end with a trailing slash - this is critical for the usage of HttpClient!
            // http://stackoverflow.com/questions/23438416/why-is-httpclient-baseaddress-not-working
            if (!endpoint.EndsWith("/"))
            {
                endpoint = endpoint + "/";
            }

            // ServiceFabric publishes ASP.NET Core projects with the endpoint "http://+:port" to listen on all IPs.
            // However, it's not possible to call the + url directly so we have to change it to localhost.
            endpoint = endpoint.Replace("+", "localhost");

            // We default to HTTP if it's not available in the endpoint.
            if (!endpoint.Contains("://"))
            {
                endpoint = "http://" + endpoint;
            }

            return new Uri(endpoint, UriKind.Absolute);
        }
    }
}