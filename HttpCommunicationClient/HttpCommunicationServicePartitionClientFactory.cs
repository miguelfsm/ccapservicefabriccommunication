﻿using System;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClient
{
    public class HttpCommunicationServicePartitionClientFactory : IHttpCommunicationServicePartitionClientFactory
    {
        private readonly ICommunicationClientFactory<HttpCommunicationClient> _communicationClientFactory;
        private readonly Uri _serviceUri;
        private readonly TargetReplicaSelector _targetReplicaSelector;
        private readonly string _listenerName;
        private readonly OperationRetrySettings _retrySettings;

        public HttpCommunicationServicePartitionClientFactory(
            ICommunicationClientFactory<HttpCommunicationClient> communicationClientFactory, 
            Uri serviceUri, 
            TargetReplicaSelector targetReplicaSelector = TargetReplicaSelector.Default,
            string listenerName = null, 
            OperationRetrySettings retrySettings = null)
        {
            _communicationClientFactory = communicationClientFactory;
            _serviceUri = serviceUri;
            _targetReplicaSelector = targetReplicaSelector;
            _listenerName = listenerName;
            _retrySettings = retrySettings;
        }

        public HttpCommunicationServicePartitionClient Create(ServicePartitionKey partitionKey = null)
        {
            return new HttpCommunicationServicePartitionClient(_communicationClientFactory, _serviceUri, partitionKey,
                _targetReplicaSelector, _listenerName, _retrySettings);
        }
    }
}
