﻿using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClient
{
    public interface IHttpCommunicationClientFactory : ICommunicationClientFactory<HttpCommunicationClient>
    {
    }
}