﻿using Microsoft.ServiceFabric.Services.Client;

namespace HttpCommunicationClient
{
    public interface IHttpCommunicationServicePartitionClientFactory
    {
        HttpCommunicationServicePartitionClient Create(ServicePartitionKey partitionKey = null);
    }
}