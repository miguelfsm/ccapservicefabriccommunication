﻿using System;

namespace HttpCommunicationClientNetStd
{
    public class CommunicationClientConfigurationNetStd
    {
        public TimeSpan RequestTimeout { get; set; } = TimeSpan.FromSeconds(5);
    }
}