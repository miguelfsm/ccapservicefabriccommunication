﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClientNetStd
{
    public class HttpCommunicationClientExceptionHandlerNetStd : IExceptionHandler
    {
        public bool TryHandleException(ExceptionInformation exceptionInformation, OperationRetrySettings retrySettings,
            out ExceptionHandlingResult result)
        {
            var ex = exceptionInformation.Exception;

            // errors where we didn't get a response from the service.
            if (ex is TaskCanceledException || ex is TimeoutException)
                return CreateExceptionHandlingRetryResult(false, ex, retrySettings, out result);

            if (ex is ProtocolViolationException)
                return CreateExceptionHandlingRetryResult(false, ex, retrySettings, out result);

            //handle HttpClient exceptions from the server response
            var webEx = ex as WebException ?? ex.InnerException as WebException;
            if (webEx != null)
                if (webEx.Status == WebExceptionStatus.Timeout ||
                    webEx.Status == WebExceptionStatus.RequestCanceled ||
                    webEx.Status == WebExceptionStatus.ConnectionClosed ||
                    webEx.Status == WebExceptionStatus.ConnectFailure)
                    return CreateExceptionHandlingRetryResult(false, webEx, retrySettings, out result);

            
            HttpStatusCode? httpStatusCode = null;
            HttpWebResponse webResponse = null;

            if (webEx != null)
            {
                webResponse = webEx.Response as HttpWebResponse;
                httpStatusCode = webResponse?.StatusCode;
            }

            if (httpStatusCode.HasValue)
            {
                if (httpStatusCode == HttpStatusCode.NotFound)
                {
                    // if exceptionInformation.getException() is known and is not transient (indicates a new service endpoint address must be resolved)
                    return CreateExceptionHandlingRetryResult(false, "HTTP 404", retrySettings, 3, out result);
                }

                if ((int) httpStatusCode >= 500 && (int) httpStatusCode < 600)
                {
                    return CreateExceptionHandlingRetryResult(true, ex, retrySettings, out result);
                }
            }

            /* if exceptionInformation.getException() is unknown (let the next ExceptionHandler attempt to handle it)
             */
            result = null;
            return false;
        }

        private bool CreateExceptionHandlingRetryResult(bool isTransient, Exception ex,
            OperationRetrySettings retrySettings, out ExceptionHandlingResult result)
        {
            var retryDelay = GetRetryDelay(isTransient, retrySettings);

            result = new ExceptionHandlingRetryResult(
                ex.GetType().Name,
                isTransient,
                retryDelay,
                retrySettings.DefaultMaxRetryCount);

            return true;
        }

        private bool CreateExceptionHandlingRetryResult(bool isTransient, string exceptionId,
            OperationRetrySettings retrySettings, int maxRetries, out ExceptionHandlingResult result)
        {
            var retryDelay = GetRetryDelay(isTransient, retrySettings);

            result = new ExceptionHandlingRetryResult(
                exceptionId,
                isTransient,
                retryDelay,
                maxRetries);

            return true;
        }

        private static TimeSpan GetRetryDelay(bool isTransient, OperationRetrySettings retrySettings)
        {
            return isTransient
                ? retrySettings.MaxRetryBackoffIntervalOnTransientErrors
                : retrySettings.MaxRetryBackoffIntervalOnNonTransientErrors;
        }
    }
}