﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClientNetStd
{
    public static class HttpCommunicationClientExtensionsNetStd
    {
        public static IServiceCollection AddServiceFabricHttpCommunicationNetStd(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null)
                throw new ArgumentNullException(nameof(services));

            services.AddTransient(x => ServicePartitionResolver.GetDefault());
            services.AddTransient<IExceptionHandler, HttpCommunicationClientExceptionHandlerNetStd>();
            services.AddSingleton<IHttpCommunicationClientFactoryNetStd, HttpCommunicationClientFactoryNetStd>();
            services
                .AddSingleton<IHttpCommunicationServicePartitionClientFactoryNetStd,
                    HttpCommunicationServicePartitionClientFactoryNetStd>(
                    (ctx) =>
                    {
                        var communicationClientFactory =
                            ctx.GetService<IHttpCommunicationClientFactoryNetStd>();
                        var serviceUri = new Uri(configuration["ConnectedServicesConfiguration:ValuesServiceUri"]);
                        return new HttpCommunicationServicePartitionClientFactoryNetStd(communicationClientFactory,
                            serviceUri);
                    }
                );

            return services;
        }
    }
}