﻿using System;
using Microsoft.ServiceFabric.Services.Client;
using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClientNetStd
{
    public class HttpCommunicationServicePartitionClientNetStd : ServicePartitionClient<HttpCommunicationClientNetStd>,
        IHttpCommunicationServicePartitionClientNetStd
    {
        public HttpCommunicationServicePartitionClientNetStd(
            ICommunicationClientFactory<HttpCommunicationClientNetStd> communicationClientFactory,
            Uri serviceUri,
            ServicePartitionKey partitionKey = null,
            TargetReplicaSelector targetReplicaSelector = TargetReplicaSelector.Default,
            string listenerName = null,
            OperationRetrySettings retrySettings = null)
            : base(communicationClientFactory,
                serviceUri,
                partitionKey,
                targetReplicaSelector,
                listenerName,
                retrySettings)
        {
            if (communicationClientFactory == null) throw new ArgumentNullException(nameof(communicationClientFactory));
            if (serviceUri == null) throw new ArgumentNullException(nameof(serviceUri));
        }
    }
}