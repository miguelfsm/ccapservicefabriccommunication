﻿using Microsoft.ServiceFabric.Services.Communication.Client;

namespace HttpCommunicationClientNetStd
{
    public interface IHttpCommunicationClientFactoryNetStd : ICommunicationClientFactory<HttpCommunicationClientNetStd>
    {
    }
}