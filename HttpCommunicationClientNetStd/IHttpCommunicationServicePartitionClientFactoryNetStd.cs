﻿using Microsoft.ServiceFabric.Services.Client;

namespace HttpCommunicationClientNetStd
{
    public interface IHttpCommunicationServicePartitionClientFactoryNetStd
    {
        HttpCommunicationServicePartitionClientNetStd Create(ServicePartitionKey partitionKey = null);
    }
}
