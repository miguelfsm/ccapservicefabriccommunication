﻿using System;
using System.Fabric;
using System.Threading;
using System.Threading.Tasks;

namespace HttpCommunicationClientNetStd
{
    public interface IHttpCommunicationServicePartitionClientNetStd
    {
        bool TryGetLastResolvedServicePartition(out ResolvedServicePartition resolvedServicePartition);
        Task<TResult> InvokeWithRetryAsync<TResult>(Func<HttpCommunicationClientNetStd, Task<TResult>> func, params Type[] doNotRetryExceptionTypes);
        Task<TResult> InvokeWithRetryAsync<TResult>(Func<HttpCommunicationClientNetStd, Task<TResult>> func, CancellationToken cancellationToken, params Type[] doNotRetryExceptionTypes);
        Task InvokeWithRetryAsync(Func<HttpCommunicationClientNetStd, Task> func, params Type[] doNotRetryExceptionTypes);
        Task InvokeWithRetryAsync(Func<HttpCommunicationClientNetStd, Task> func, CancellationToken cancellationToken, params Type[] doNotRetryExceptionTypes);
        TResult InvokeWithRetry<TResult>(Func<HttpCommunicationClientNetStd, TResult> func, params Type[] doNotRetryExceptionTypes);
        void InvokeWithRetry(Action<HttpCommunicationClientNetStd> func, params Type[] doNotRetryExceptionTypes);
    }
}
