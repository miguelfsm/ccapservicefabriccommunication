# README #

### Update on 2018.04.28 ###

I wasn't very happy with the first iteration of it, as it was creating an instance of Service Fabric ServicePartitionClient in the controller method, so in a real project, the ammount of repeated code would grow rapidly. So I refactored and created a few more bits of code. Also, HttpCommunicationClientFactory is not easy to test because the base class has all the meaningfull methods as protected. Some say we shouldn't work around to test these, but I created a wrapper to be able to do so. So the following are the changes done today:

- Created ServicePartitionClient factories to have the WebAPI controllers easilly testable (both .NET Classic and Standard). Configuration of xyzServicePartitionClientFactory is injected in the Startup class through existing extension method. We could have on Factory per service that we need to call. With this, the controllers only receive on intance of that factory throught it's interface. The new factories are also configured as a singleton.
- Added Tests to the HttpCommunicationClient and Factories. 
- No tests are added to the controller because they are just for demo purposes but, since now we have all the dependencies injected, that would be very easy to do in a real project.

---------------------------------------

### What is this repository for? ###

* This repository contains an implementation of an HTTP Service Fabric Communication client.


### How do I get set up? ###

* Summary of set up

There are very few things needed, but obviously the latest Service Fabric SDK is needed.

* Configuration

A couple of configuration parameters were added to the appsettings.json file. These control the HTTP request timeout value, the service URI and the service endpoint used for demo purposes.

* Dependencies

All external dependencies were added through nuget packages

* Deployment instructions

The easiest way to get this deployed locally is though Visual Studio. Just load the solution, build and run. Obviously the project also has a deployment script that can be used.


### Project structure, client registration and demo endpoints ###

This solution contains a Service Fabric application with 2 WebApi services, namely, CallingWebApp and ValueService. ValueService is a plain vanilla WebApi project with just one controller that returns hardcoded data. Its purpose is being an HTTP service that can be called from CallingWebApp using the implemented HTTP Service Fabric Communication Client.

This solution has two implementations of the communication client, one targeting .NET classic 4.6.1 and another one targeting .NET Standard 2.0. These are pretty much the same code (except class names) in separate projects targeting different frameworks. In real live this would only be needed if we would need to target an older version of .NEt classic 4.6.1 or ASP.NET Core 1.x.

To have a way to demo the use of these two clients, two controllers were created in the CallingWebApp to use each one of them. Their Factories were configured and injected using the Microsoft.Extensions.DependencyInjection library. This can also be easily being done with Autofac. The factories were registered as Singletone, as they have some operations that are heavy, and they cache the clients created, so creating a new factory each time would be very inefficient.

Both clients were registerid and work inside the same service. This is not surprising but it's worth metioning as one can migrate libraries to .NET Standard one by one and have them running together with .NET classic libraries in the same service. This was somehow possible in the past but since the release of .NET Standard/ ASP.NET Core 2.0 and the latest release of Service Fabric SDK this became really an option.

The endpoints that can be called in a browser to see this working are the following:
* .NET classic implementation: http://localhost:8560/api/SomeOperation
* .NET Standard implementation: http://localhost:8560/api/NetStdOperation


### What do I need to do to implement an HTTP Service Fabric Communication Client ###

MSDN is a great source of information. Checking the information about Service Fabric and communication clients, one realizes that there are basically three interfaces that must be implemented in order to have a custom communication client. These are the following:

* ICommunicationClient: Defines the interface that represents the communication client for reliable services.

* ICommunicationClientFactory<TCommunicationClient>: Defines the interface that must be implemented to provide a factory for communication clients to talk to a service fabric service. There's a base abstract class provided by Microsoft that does the heavy work for us.

* IExceptionHandler: Defines the interface for handling the exceptions encountered in communicating with service fabric services. In here we try to figure out if an exception is something temporary, and in that case communication should be retried, or permanent. In both cases the client should be informed by returning specific classes that the framework provides.

And that's basically it. For HTTP is straightforward to implement. For other types of communication, it might require a bit more work, specially to control is the client is still valid.


[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)